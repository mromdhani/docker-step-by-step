# Docker Compose
---
Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. To learn more about all the features of Compose, [see the list of features](https://docs.docker.com/compose/#features).

Compose works in all environments: production, staging, development, testing, as well as CI workflows. You can learn more about each case in [Common Use Cases](https://docs.docker.com/compose/#common-use-cases).

Using Compose is basically a three-step process:

1. Define your app’s environment with a `Dockerfile` so it can be reproduced anywhere.

2. Define the services that make up your app in `docker-compose.yml` so they can be run together in an isolated environment.

3. Run `docker-compose up` and Compose starts and runs your entire app.


## Installation
#### Under Windows and MacOS
If you installed docker via docker-toolbox or docker desktop for windows, docker-compose is already installed and functional.

Docker for Mac and Docker Toolbox already ship Docker Compose with the other Docker applications, so everything is ready to go.

Check the version of Docker Compose
```
$ docker-compose version
```

#### Under GNU / Linux
We have 3 possibilities to install docker-compose under GNU / Linux, with pip, by hand, or with a container. We will only see here the installation via pip.

#### Installation via pip
pip is a package manager for python, which allows the installation of applications, or libraries. It's a bit like apt-get.

We first install pip, under debian it gives:

```shell
$ apt-get install python-pip
```
Now just run this command:

```shell
$ pip install docker-compose
```
We can test:

```
$ docker-compose version
```
## Getting started with Docker Compose
To get help, simply run:

```shell
$ docker-compose -help
```
This command will list out the commands which docker compose can perform.
Now let us take a simple docker compose file as an example (`docker-compose.yaml`). Here is its contents.

```yaml
version: '3'
services:
  web:
    image: ngnix
  database:
    image: redis
```
Above docker compose file is a bare minimum file to understand the basic content inside the compose file.

We can check the validity of the file by using command:

```shell
$ docker-compose config
```
Now let us run the compose file using command:

```shell
$ docker-compose up -d
```
It will start all services with single command.
To list out the running containers created by compose file, run:

```shell
$ docker-compose ps
```
We can bring down the application at any time with the following command:

```shell
$ docker-compose down
```
We can also assign a different port to nginx, for example 8181.

To do so, just define the port in compose file as shown below:
```yaml
version: '3'
services:
  web:
    image: ngnix
    ports: 
    - 8181:80/tcp
  database:
    image: redis
```
Restart the services:
```shell
$ docker-compose up -d
```
Open the browser and verify if it is running on 8181 port.

## Scaling the services
If you want to scale the service, you can do it using command:

```shell
$ docker-compose up -d --scale database=3
```

Chech that there as are three database containers using command:
```shell
$ docker-compose ps
```
To display running services, run:

```shell
$ docker-compose top
```
To stop, start. restart the whole service at once, the commands would be:
```shell
$ docker-compose stop
```
```shell
$ docker-compose start
```
```shell
$ docker-compose restart
```
We can view logs of the services using command:

```shell
$ docker-compose logs
```

## Docker Compose Example with Dockerfile
I will create two Docker containers using Docker compose. One docker container has Apache web server with our dummy application file (Dokerfile). The second will have MySQL database instance.

#### Step 1 – Create Directory Structure
First of all, create a directory structure. Here webapp is our web application directory. Also, create a `index.html` in webapp directory for testing.

```shell
$ mkdir dockercompose && cd dockercompose
$ mkdir webapp
$ echo "<h2>It Works</h2>" > webapp/index.html

```
####  Step 2 – Create Dockerfile for Webapp
Now create a Dockerfile in webapp directory to create a customized image for your application including Apache web server.

```shell
$ vim  webapp/Dockerfile
```
add following content

```shell
FROM ubuntu

RUN apt-get update && apt-get install -y apache2

COPY index.html /var/www/html/
WORKDIR /var/www/html
CMD ["apachectl", "-D", "FOREGROUND"]
EXPOSE 80
```
####  Step 3 – Create Docker Compose File
Finally create a docker compose configuration file (`docker-compose.yml`) file in current directory. This will define all the containers will be used in your current setup.

```SHELL
$ vim  docker-compose.yml
```
add the following content.

```yaml
version: '3'
services:
  db:
     image: mysql
     container_name: mysql_db
     restart: always
     environment:
        - MYSQL_ROOT_PASSWORD="secret"
  web:
    image: apache
    build: ./webapp
    depends_on:
       - db
    container_name: apache_web
    restart: always
    ports:
      - "8080:80"

```
The docker compose file has settings for two containers. The first container is for mysql database server and the second is for web server. The web container will run our application on Apache server. As this is customized we have defined build directory to webapp.

####  Step 4 – Build Webapp Image
Now, build an image using the following command. This will create an image named apache using Dockerfile and contents from webapp directory.

```shell
$ docker-compose build
```
For web container it uses `webapp/Dockerfile` to build an image.

####  Step 5 – Launch Docker Containers
Finally launch your containers using docker-compose up command. Use -d switch to run them in daemon mode.

```shell
$ docker-compose up -d
```
You can access your web application running on the apache_web container by accessing your docker host on port 8080. For example, `http://dockerhost:8080/` where dockerhost is IP or hostname of your Docker host machine.

####  Step 6 – Update Content in Web Application
Let’s make a change in your web application. I have added some more content to webapp/index.html file as following.
```shell
$ echo "<h1>Welcome to Docker Compose Tutorial </Hh1>" >> webapp/index.html
```
Sop the container and restart them
```shell
$ docker-compose down
```
```shell
$ docker-compose up -d --build
```
Browse again `http://dockerhost:8080/`
