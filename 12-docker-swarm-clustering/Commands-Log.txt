Windows 10 /Hyper Configuration : Add an External virtual switch
1.Launch Hyper-V Manager 
2. Click Virtual Switch Manager in the right-hand menu 
3. Click Create Virtual Switch of type External 
4. Give it the name myswitch, and check the box to share your host machine's active network adapter 


docker-machine create --driver hyperv manager1

docker-machine ls

docker-machine ip manager1

docker-machine ssh manager1 "docker swarm init --advertise-addr MANAGER_IP"

docker-machine ssh manager1 "docker node ls"

docker swarm join --token SWMTKN-1-4v2ia2l7s8ddbjj6j1ll7hgbwjjnp5ftd2ceqmizhdxy5tavpt-20afcsi6t15spkpfw7phsvkun 192.168.0.6:2377

docker-machine ssh manager1 "docker node ls"

docker service create --replicas 3 -p 80:80 --name myngnix nginx

docker service ls

docker service ps myngnix

Scale Up and Down
docker service scale myngnix=2

docker service ps myngnix

docker node inspect nodename

docker node inspect workerName

Shutdown node:
docker node update --availability drain worker1

Update Service:
docker service update --image nginx:1.14.0 mynginx

Remove Service
docker service rm serviceName

Other tools:
docker swarm leave # to leave the swarm
docker-machine stop machineName # to stop the machine
docker-machine rm machineName # to remove the machine






