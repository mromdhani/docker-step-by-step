package be.businesstraining.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/greetings/{name}")
	public String greeting(@PathVariable String name) {
		return "Spotify Maven Plugin - Hello  " + name;
	}

}
