Docker Swarm
---
A **swarm** is a group of machines that are running Docker and joined into a cluster 
Docker Swarm is a tool for Container Orchestration.
Let's take an example. Suppose youou have 100 containers and you need to do: 
- Health check on every container
- Ensure all containers are up on every system
- Scaling the containers up or down depending on the load
- Adding updates/changes to all the containers

* A swarm is a group of machines that are running Docker and joined into a cluster. After that has happened, you continue to run the Docker commands you’re used to, but now they are executed on a cluster by a **swarm manager**. The machines in a swarm can be physical or virtual. After joining a swarm, they are referred to as **nodes**.

Swarm managers can use several strategies to run containers, such as "emptiest node" – which fills the least utilized machines with containers. Or “global”, which ensures that each machine gets exactly one instance of the specified container. You instruct the swarm manager to use these strategies in the Compose file, just like the one you have already been using.

Swarm managers are the only machines in a swarm that can execute your commands, or authorize other machines to join the swarm as workers. **Workers** are just there to provide capacity and do not have the authority to tell any other machine what it can and cannot do.

Up until now, you have been using Docker in a single-host mode on your local machine. But Docker also can be switched into swarm mode, and that’s what enables the use of swarms. Enabling swarm mode instantly makes the current machine a swarm manager. From then on, Docker will run the commands you execute on the swarm you’re managing, rather than just on the current machine.

![swarm](images/docker-swarm-docker.jpg)
**Orchestration** means managing and controlling multiple docker containers as a single service
Tools available - Docker Swarm, Kubernetes, Apache Mesos

## Step 1 :  Set Up a Swarm

AA swarm is made up of multiple nodes, which can be either physical or virtual machines. The basic concept is simple enough: run docker swarm init to enable swarm mode and make your current machine a swarm manager, then run docker swarm join on other machines to have them join the swarm as workers. Choose a tab below to see how this plays out in various contexts. We’ll use VMs to quickly create a two-machine cluster and turn it into a swarm.

- Create a Cluster

VMS ON YOUR LOCAL MACHINE ([WINDOWS 10](https://docs.docker.com/get-started/part4/#localwin))
First, quickly create a virtual switch for your virtual machines (VMs) to share, so they will be able to connect to each other.

1. Launch Hyper-V Manager
2. Click Virtual Switch Manager in the right-hand menu
3. Click Create Virtual Switch of type External
4. Give it the name myswitch, and check the box to share your host machine’s active network adapter
Create one machine as manager and others as workers
```shell
$docker-machine create --driver hyperv manager1  # --hyperv-virtual-switch "myswitch" is optional if you have only one external virtual switch. 
```  

## Step 2 :  Check machine created successfully
```shell
$ docker-machine ls
```    
```
$ docker-machine ip manager1
```
## Step 3 :  Initialize Docker Swarm    
```shell
$ docker-machine ssh manager1 "docker swarm init --advertise-addr MANAGER_IP"
Swarm initialized: current node (lrv8y1qwtsov1z84wnkwoz8i9) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-1zd5df6uwifms534if6g1jvcdqdlh1wi7df6nrvvpdptiwggt4-4uzlgw3juq04xoqyrjs2c0dre 192.168.0.6:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```    
Check that the swar contains the manager node.
```shell
$ docker-machine ssh manager1 "docker node ls"
```
## Step 4 :  Join workers in the swarm
    In the host command prompt, run the following command:
```shell
$ docker swarm join --token SWMTKN-1-4v2ia2l7s8ddbjj6j1ll7hgbwjjnp5ftd2ceqmizhdxy5tavpt-20afcsi6t15spkpfw7phsvkun 192.168.0.6:2377
```
This will give command to join swarm as worker. Check that the worker is registered correctly.
```shell
docker-machine ssh manager1 "docker node ls"
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
e772ql2qbh63dixn1cuptal5n     docker-desktop      Ready               Active                                  19.03.5
nswfy8myp7lx1a9x92olrchht *   manager1            Ready               Active              Leader              19.03.5
```

## Step 5 :  Run containers on Docker Swarm
SSH onto manager1  (`docker-machine ssh manager1`)and create the following service.
```shell
$ docker service create --replicas 3 -p 80:80 --name myngnix nginx
```    
Check the status of the deployed services:
```shell
$ docker service ls
```    
```shell
$ docker service ps myngnix
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
f4digm5rng7r        myngnix.1           nginx:latest        manager1            Running             Running 2 minutes ago
n41b2t7mqg4l        myngnix.2           nginx:latest        docker-desktop      Running             Running 2 minutes ago
vyjzcirrz2ir        myngnix.3           nginx:latest        docker-desktop      Running             Running 2 minutes ago
```
Check the service running on all nodes (You see that one instance is on the manager and two instances are on the agent)
Check on the browser by giving ip for all nodes. Browse localhost:80 and 192.168.0.6:80

 ## Step 6 :  Scale service up and down
 On manager node 
```shell
$ docker service scale myngnix=2
```
Then check the services running.
```shell
$ docker service ps myngnix
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
f4digm5rng7r        myngnix.1           nginx:latest        manager1            Running             Running 6 minutes ago
n41b2t7mqg4l        myngnix.2           nginx:latest        docker-desktop      Running             Running 6 minutes ago
```
Inspecting Nodes (this command can run only on manager node)
```shell
$ docker node inspect nodename
```
```shell
$ docker node inspect self
```
```shell
$ docker node inspect workerName
```
## Step 7 : Shutdown node
```shell
docker node update --availability drain worker1
```

## Step 8  :  Update service
```shell
$ docker service update --image nginx:1.14.0 mynginx
```
## Step 9 :  Remove service
```shell
$ docker service rm serviceName
```
**Other Swarm Tools:**
```shell
$ docker swarm leave # to leave the swarm
```
```shell
$ docker-machine stop machineName # to stop the machine
```
```shell
$ docker-machine rm machineName # to remove the machine
```