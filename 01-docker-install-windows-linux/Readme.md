# Installation of Docker
---
Docker Engine is available on a variety of Linux platforms, Mac and Windows through Docker Desktop, Windows Server, and as a static binary installation.

## Installation of Docker on Linux

### Install Docker on Ubuntu Using Default Repositories

- Update Software Repositories

  ```shell
    $ sudo apt-get update
  ```
- Next, it’s recommended to uninstall any old Docker software before proceeding.
  ``` shell
    $ sudo apt-get remove docker docker-engine docker.io
  ```
- Install Docker
  ```shell 
    $ sudo apt install docker.io
  ```  
- Start and Automate Docker
  ```shell
    $ sudo systemctl start docker
    $ sudo systemctl enable docker # so that it will automatically start on boot
  ```
- (Optional): Check Docker Version
  ```shell 
  $ docker --version
  ```
### Alternative: Install Docker from Official Repository

- Update the local database with the command:

  ```shell
  $ sudo apt-get update
  ```
- Download Dependencies
  ```shell
  $ sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
  ```
To clarify, here's a brief breakdown of each command:
    - _apt-transport-https_: Allows the package manager to transfer files and data over https
    - _ca-certificates_: Allows the system (and web browser) to check security certificates
    - _curl_: This is a tool for transferring data
    - _software-properties-common_: Adds scripts for managing software

- Add Docker's GPG Key
  ```shell
  $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
  ```
- Install the Docker Repository
  ```shell
    $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"   
    ```
  The command "$(lsb_release –cs)" scans and returns the codename of your Ubuntu installation – in this case, Bionic. Also, the final word of the command – stable– is the type of Docker release.

- Update repositories
    ```shell
  $ sudo apt-get update
  ```  
-  Install Latest Version of Docker
    ```shell
  $ sudo apt-get install docker-ce
  ```
- (Optional): Check Docker Version
  ```shell 
  $ docker --version
  ```
## Install Docker on Windows
To install Docker on Windows there are two options:
- [Docker Toolbox](https://github.com/docker/toolbox/releases)):  Executable that installs everything, Virtualbox, a VM (docker machine), and clients.
- [Docker Desktop for Windows](https://hub.docker.com/?overlay=onboarding) : Like Docker Toolbox, but better, uses Hyper-V instead of Virtualbox. **Only compatible from windows 10 Pro**.

We will detail the installation of Docker Desktop for Windows in this step.

The following hardware prerequisites are required to successfully run Client Hyper-V on Windows 10:
  - 64-bit processor with Second [Level Address Translation (SLAT)](http://en.wikipedia.org/wiki/Second_Level_Address_Translation).
  - 4GB system RAM 
  - BIOS-level hardware virtualization support must be enabled in the BIOS settings. For more information, see Virtualization.
- Computer with Internet access
- Hyper-V and Containers Windows features must be enabled.
  
To check if Hyper-V is enabled go to **Start** -> **Turn Windows features on or off**
![Capture](images/turn-windows-features.png)
Check if **Hyper-V Hypervisor**  is enabled
![capture](images/hyperv-hypervisor.png)

## Docker Desktop Installation
Open your favourite web browser and go to www.docker.com web page and click on **Sign In**.
![capture](images/docker-website.png)
Create a new account and login to it. Once you are logged in, you should be able to see the welcome page. Click on **Download Docker Desktop for Windows**.
![capture](images/download-docker-website.png)
Once the file is downloaded, double click on it so installation can start. Use default options as on the screen below.
![capture](images/installing-docker-configuration.png)
Click **Ok** and wait a few minutes for the installation to be completed.
![capture](images/installing-docker.png)
Once Docker is installed you need to log out and log in again. For best experience restart your computer.
![capture](images/installing-docker-logout.png)

## Test your installation
1. Open a terminal window (Command Prompt or PowerShell, ).

2. Run `docker --version` to ensure that you have a supported version of Docker:

   ``` shell
   $ docker --version
   Docker version 19.03.5, build 633a0ea 
   ```

3. Pull the [hello-world image](https://hub.docker.com/r/library/hello-world/) from Docker Hub and run a container:

   ``` shell
  $ docker run hello-world

   docker : Unable to find image 'hello-world:latest' locally
   latest: Pulling from library/hello-world
   1b930d010525: Pull complete
   Digest: sha256:c3b4ada4687bbaa170745b3e4dd8ac3f194ca95b2d0518b417fb47e5879d9b5f
   Status: Downloaded newer image for hello-world:latest

   Hello from Docker!
   This message shows that your installation appears to be working correctly.
   ...
   ```
4. List the `hello-world` image that was downloaded from Docker Hub:

   ```shell
   $ docker image ls
   ```
5. List the `hello-world` container (that exited after displaying "Hello from Docker!"):

   ```shell
   $ docker container ls --all
   ```
6. Explore the Docker help pages by running some help commands:
   ```
   $ docker --help
   $ docker container --help
   $ docker container ls --help
   $ docker run --help
   ```
## Explore the application
In this section, we demonstrate the ease and power of Dockerized applications by running something more complex, such as an OS and a webserver.

1. Pull an image of the [Ubuntu OS](https://hub.docker.com/r/_/ubuntu/) and run an interactive terminal inside the spawned container:
   ```shell
    $ docker run --interactive --tty ubuntu bash
    docker : Unable to find image 'ubuntu:latest' locally
    latest: Pulling from library/ubuntu
    22e816666fd6: Pull complete
    079b6d2a1e53: Pull complete
    11048ebae908: Pull complete
    c58094023a2e: Pull complete
    Digest: sha256:a7b8b7b33e44b123d7f997bd4d3d0a59fafc63e203d17efedf09ff3f6f516152
    Status: Downloaded newer image for ubuntu:latest
   ```
2. You are in the container. At the root # prompt, check the hostname of the container:
    ``` shell
    root@8aea0acb7423:/# hostname
    8aea0acb7423
    ```
  Notice that the `hostname` is assigned as the container ID (and is also used in the prompt).

3. Exit the shell with the exit command (which also stops the container):
    ```shell
    root@8aea0acb7423:/# exit 
    ```
4. List containers with the `--all` option (because no containers are running).

The `hello-world` container (randomly named, `relaxed_sammet`) stopped after displaying its message. The `ubuntu` container (randomly named, `laughing_kowalevski`) stopped when you exited the container.

    ```shell
    $ docker container ls --all

    CONTAINER ID    IMAGE          COMMAND     CREATED          STATUS                      PORTS    NAMES
    8aea0acb7423    ubuntu         "bash"      2 minutes ago    Exited (0) 2 minutes ago             laughing_kowalevski
    45f77eb48e78    hello-world    "/hello"    3 minutes ago    Exited (0) 3 minutes ago             relaxed_sammet
    ```
5. Pull and run a Dockerized [nginx](https://hub.docker.com/_/nginx/) web server that we name, `webserver`:

    ```shell
    $ docker run --detach --publish 80:80 --name webserver nginx

    Unable to find image 'nginx:latest' locally
    latest: Pulling from library/nginx

    fdd5d7827f33: Pull complete
    a3ed95caeb02: Pull complete
    716f7a5f3082: Pull complete
    7b10f03a0309: Pull complete
    Digest: sha256:f6a001272d5d324c4c9f3f183e1b69e9e0ff12debeb7a092730d638c33e0de3e
    Status: Downloaded newer image for nginx:latest
    dfe13c68b3b86f01951af617df02be4897184cbf7a8b4d5caf1c3c5bd3fc267f
    ```
 6. Point your web browser at `http://localhost` to display the nginx start page. (You don’t need to append `:80` because you specified the default HTTP port in the `docker` command.)

![capture](images/nginx-homepage.png)

7. List only your running containers:
    ```shell
      $ docker container ls

      CONTAINER ID    IMAGE    COMMAND                   CREATED          STATUS          PORTS                 NAMES
      0e788d8e4dfd    nginx    "nginx -g 'daemon of…"    2 minutes ago    Up 2 minutes    0.0.0.0:80->80/tcp    webserver
    ``` 
 8. Stop the running nginx container by the name we assigned it, webserver:
    ```shell
    $  docker container stop webserver
    ```
9. Remove all three containers by their names -- the latter two names will differ for you:
    ```
    $ docker container rm webserver laughing_kowalevski relaxed_sammet
    ```
### Settings of Docker
In the taskbar you should be able to see Docker icon and hovering over it should show the status of Docker. After right click on the Docker icon you can go to Settings for configuration of Docker VM:
![capture](images/docker-desktop-menu.png)
In the **Advanced** settings tab, you can configure the CPU and Memory configuration of Docker.
![capture](images/docker-desktop-advanced.png)
Note: all of the above settings, are applied to VM visible in **Hyper-V Manager**.
![capture](images/hyperv-docker-desktop.png)
After right click on **DockerDesktopVM** and selecting **Settings**, the following window will appear.
![capture](images/hyperv-docker-dekstop-settings.png)