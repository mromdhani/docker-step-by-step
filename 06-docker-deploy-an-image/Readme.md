# Deploy a Docker Image
---
In this step, we will be pushing our built image to the registry so that we can use it anywhere. The Docker CLI uses Docker’s public registry by default.

## Deployment to Docker Hub

- Log into the Docker public (Docker Hub) registry from your local machine.
```shell
$ docker login
```
- **Tag the image**: It is more like naming the version of the image. It's optional but it is recommended as it helps in maintaining the version(same like ubuntu:16.04 and ubuntu:17.04)

```shell
$ docker tag my-web-app mromdhani/get-started:01
```
- Publish the image: Upload your tagged image to the repository: Once complete, the results of this upload are publicly available. If you log into Docker Hub, you will see the new image there, with its pull command.
```shell
$ docker push  mromdhani/get-started:01
```
That's it, you are done. Now you can go to Docker hub and can check about it also. You published your first image.
![capture](images/publishing.png)


```sheell
>docker push  mromdhani/get-started:01
The push refers to repository [docker.io/mromdhani/get-started]
aebb2652aa30: Pushed 
7cdbefd28fe4: Pushed
8aa41c245e90: Pushed
668c0ea1e1a4: Pushed
f55aa0bd26b8: Mounted from library/ubuntu
1d0dfb259f6a: Mounted from library/ubuntu
21ec61b65b20: Mounted from library/ubuntu
43c67172d1d1: Mounted from library/ubuntu 
01: digest: sha256:071da23f585371e8c315b3e056103074e4e7d3e12f80348a00c9a4df7efd6120 size: 1985
```

## Creation of a private Docker registry
Docker hub (hub.docker.com) is the primary source for the public images.  DockerHub only allows 1 private image to be stored in the Free plan, 5 images for 7$ and 10 for 12$ which is decent price for production projects, but for personal hobby projects may be a lot.

You can have a 5$ server on DigitalOcean for personal needs, which you want also to use as a docker registry, and CI for my private projects.

There are a few reasons setting up a loal docker registry :

- Total control of our registry and repositories
- We need to set up a local network and to easily distribute images throughout it, to save the bandwidth
- We have a closed network without internet access
- Setting up a local CI build server that uses that registry
- We don't want to pay some crazy plans to cloud providers to host our repositories
Follow the steps below to create your private Docker registry:

###Steps
- Create a directory to permanently store images.
    - Navigate to `C:\ drive` and create a folder with the name of localhub (mkdir localhub). For Linux, create the same folder under `/home`.
        ```shell
            mkdir localhub 
        ```
    - Navigate to `C:\localhub` folder in windows or `/home/localhub` in Linux and create a subfolder with the name of "`registry`".
        ```shell
            cd localhub 
            mkdir registry 
        ```
- Pull the registry image from DockerHub.
    - Type the following command (Windows/Linux)to pull the registry image from the docker hub:
      
        ```shell
        docker pull registry
        ```
- (_This step is optional_)
   Before going to the docker registry set up, I want to set up a _meaningful local domain name for your private registry instead of using localhost_ but this step is completely optional. I prefer to have the local domain as `hub.docker.local`. To configure the local domain in windows and Linux, do the following steps:
    - Windows:
Open an elevated Notepad in docker host.
Open the `C:\Windows\System32\drivers\etc\hosts` file in the Notepad and add this `127.0.0.1 hub.docker.local`as an entry in it.
Save and close the file.
   
 - Linux:
Open a terminal in docker host and type `nano /etc/hosts` or `vi /etc/hosts`
Add this `127.0.0.1 hub.docker.local` as an entry in it.
Save and close the file.
- Spin up a container with the docker registry image
    ``` shell
    Windows: 
    docker run -d -p 5000:5000 -v C:/localhub/registry:/var/lib/registry --restart=always --name hub.local registry
    Linux:
    docker run -d -p 5000:5000 -v /home/localhub/registry:/var/lib/registry --restart=always --name hub.local registry 
    ```

    _Docker registry uses the **5000 port as default**. `--restart=always` flag is enable the auto start after the docker restarted. `-v` will bind the given host folder with container file system.
    Docker might ask you for the user name and password to share the localhub folder with container if you have not setup the share folder already in docker._
    
    Ensure the registry container is up and running:
    ```shell
    docker ps  # or docker container ls
    ```
- The next step is to prepare the docker image and push to our private registry. You can use any one of the existing images to quickly understand the concepts of the Docker Registry. Alpine is one of the lite weight Linux distribution(~5MB) so you can use this image for a quick assessment:
  ```shell
   docker pull alpine
    ```
- Create a tag to alpine Linux with `hub.docker.local:5000/my-alpine`
    ```shell
    docker tag alpine hub.docker.local:5000/my-alpine
    ```
    This creates an additional tag on an existing alpine image. Tag format will be like `registry hostname: port/new name`. Docker will extract the location from the given tag while pushing to your private registry.
- Push the my-alpine image to your private registry:
  
    ```shell
    docker push hub.docker.local:5000/my-alpine
    ```
- Remove the alpine and its tagged version to ensure docker is pulling the image from your private registry instead of docker hub.

    ```shell
    docker rmi hub.docker.local:5000/my-alpine 
    docker rmi alpine
    ```
- Now, pull the `my-alpine` image from your private registry
   ```shell
   docker pull hub.docker.local:5000/my-alpine
    ```
- Spin up a container with newly downloaded image and ask the container to list out its root directory.
   ```shell
     docker run hub.docker.local:5000/my-alpine ls
    ```
- You can check registry catalog on this http://hub.docker.local:5000/v2/_catalog address.

   ![catalogue](images/localcatalogue.png)

- (Optional)Stop and remove the registry container and image 
    ```shell
    docker container stop hub.local
    docker container rm hub.local 
    ```
- (Optional)Finally, remove the my-alpine image as well.
    ```shell
    docker ps -a
    docker container rm container_id
    docker rmi hub.docker.local:5000/my-alpine
    ```