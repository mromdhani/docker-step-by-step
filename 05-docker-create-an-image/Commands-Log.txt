Step 1

mkdir myapp

cd myapp

create Dockerfile, initilialize it as follows

FROM ubuntu:18.04
MAINTAINER myname <me@mydomain.io>
RUN apt-get update && apt-get install -y apache2 && apt-get clean && rm -rf /var/lib/apt/lists/*
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR   /var/log/apache2
ENV APACHE_PID_FILE  /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR   /var/run/apache2
ENV APACHE_LOCK_DIR  /var/lock/apache2
ENV APACHE_LOG_DIR   /var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR
COPY index.html /var/www/html
EXPOSE 80
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]

Create index.html and initialize it as follows
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First APP</title>
</head>
<body>
    <h1>I am served by a Dockerized app ...</h1>
</body>
</html>

Step 2
docker build -t my-web-app .


Step 3
docker run --name my_first_apache_instance -p 80:80 -i -t my-web-app
