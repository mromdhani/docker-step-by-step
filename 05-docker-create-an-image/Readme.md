# Creating a Docker Image
---
Docker makes it easier to create and deploy applications in an isolated environment. A Dockerfile is a script that contains collections of commands and instructions that will be automatically executed in sequence in the docker environment for building a new docker image.

In this step, I will show you how to create your own docker image with a dockerfile. 

## Dockerfile commands
A dockerfile is a script which contains a collection of dockerfile commands and operating system commands (ex: Linux commands).
Below are some dockerfile commands you must know:

- **FROM**
The base image for building a new image. This command must be on top of the dockerfile.

- **MAINTAINER**
Optional, it contains the name of the maintainer of the image.

- **RUN**
Used to execute a command during the build process of the docker image.

- **CMD**
Used for executing commands when we build a new container from the docker image.

- **EXPOSE**
Opens a TCP port.

- **ENV**
Define an environment variable.

- **ARG**
Like ENV bu is only available during the build of a Docker image (RUN etc), not after the image is created and containers are started from it (ENTRYPOINT, CMD).

- **COPY**
Copy a file from the host machine to the new docker image.

- **ADD**
Copy a file from the host machine to the new docker image. There is an option to use a URL for the file, docker will then download that file to the destination directory.

- **ENTRYPOINT**
Define the default command that will be executed when the container is running. This command is not overridable.

    >**RUN vs CMD vs ENTRYPOINT**
     - **RUN** executes command(s) in a new layer and creates a new image. E.g., it is often used for installing software packages.
     - **CMD** sets default command and/or parameters, which can be overwritten from command line when docker container runs.
     - **ENTRYPOINT** configures a container that will run as an executable.

- **WORKDIR**
This is directive for CMD command to be executed.

- **USER**
Set the user or UID for the container created with the image.

- **VOLUME**
Enable access/linked directory between the container and the host machine.

## Creating an Hello World Image
- Create a folder named `Hello-world-image`. 
   ```shell
  $ mkdir hello-world-image (create our app folder)
   ```
- Move into that folder and create a file named `Dokerfile`. 
  ```shell
  $ cd hello-world-image (lets go into our folder)
  $ code Dockerfile #Create a file called Dockerfile using the VS Code Editor
  ```
- Paste the following content in the `Dokerfile` file. 
  ```shell
  FROM alpine
  CMD ["echo", "Hello World, this is my image !"]
  ```
  This will instruct Docker to build an image based on Alpine (`FROM`), a minimal distribution for containers, and to run a specific command (`CMD`) when executing the resulting image.
- Build and run it:
  ```shell
  $ docker build -t hello .
  $ docker run --rm hello
  ```
  This will output:
  ``` shell
  Hello World, this is my image !
  ```
**ENTRYPOINT vs CMD**
  If you only specify `CMD` then docker will run that command using the default `ENTRYPOINT`, which is /bin/sh -c.  If you specify both, then the ENTRYPOINT specifies the executable of your container process, and CMD will be supplied as the parameters of that executable.

For example if your Dockerfile contains
```shell
FROM alpine
CMD ["/bin/date"]
```
Then you are using the default `ENTRYPOINT` directive of `/bin/sh -c`, and running `/bin/date` with that default entrypoint. The command of your container process will be `/bin/sh -c /bin/date`. Once you run this image then it will by default print out the current date.
```shell
$ docker run test /bin/hostname
ce0274ec8820
```
If you specify an ENTRYPOINT directive, Docker will use that executable, and the CMD directive specifies the default parameter(s) of the command. So if your Dockerfile contains:
```shell
FROM alpine
ENTRYPOINT ["/bin/echo"]
CMD ["Hello"]
```
Then running it will produce
```shell
Hello
```
You can provide different parameters if you want to, but they will all run `/bin/echo`
```shell
$ docker run --rm test Hi
Hi
```
If you want to override the entrypoint listed in your Dockerfile (i.e. if you wish to run a different command than echo in this container), then you need to specify the `--entrypoint` parameter on the command line:

```shell 
$ docker run --entrypoint=/bin/hostname test
b2c70e74df18
```
  > Generally you use the `ENTRYPOINT` directive to point to your main application you want to run, and `CMD` to the default parameters.

# Dockerizing Java Applications  

#### Simple Java Console application (05-docker-create-image-java-01-hello)
Let's create first a simple Java Console application. We adhere to the Apache Maven Conventions.
An example of Dockerfile is as follows :
```shell
FROM openjdk:8-jdk-alpine

COPY ./target/*.jar /tmp

WORKDIR /tmp

ENTRYPOINT ["java","-jar","HelloApp.jar"]
```

#### Simple Spring Boot application (05-docker-create-image-java-02-hello-springboot)
Let's create first a simple Java Console application. We adhere to the Apache Maven Conventions. An example of Dockerfile is as follows. It does not differ from the one of a simple Java application.

```shell
FROM openjdk:8-jre-alpine

COPY ./target/*.jar /tmp

WORKDIR /tmp

ENTRYPOINT ["java","-jar","greetings-app-springboot.jar"]
```

#### Simple Spring Boot application with %aven Docker Plugin(05-docker-create-image-java-03-hello-springboot-maven)
We are changing here the `pom.xml` in order to let maven generate the Docker image. The plugin is added as follows
=========================================
```shell
<plugin>
				<!-- Navigate to the project folder and type following command you will 
					be able to create image and run that image: $ mvn clean package docker:build -->

				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>1.2.2</version>
				<configuration>
					<imageName>${project.build.finalName}</imageName>
					<baseImage>java</baseImage>
					<entryPoint>["java", "-jar", "/${project.build.finalName}.jar"]</entryPoint>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.jar</include>
						</resource>
					</resources>
				</configuration>
			</plugin>
```
The following maven command will generate the image
```shell
mvn clean package docker:build 
```

