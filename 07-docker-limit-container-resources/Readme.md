# Limiting the container resources
---

## Limiting the memory consumption

To limit the memory, we will use 3 options:
- _-m --memory_: We choose the memory limit (ex: -m 200M)
- _--memory-swap_: We choose the memory limit + swap (ex: -m 200M --memory-swap 1G, this limits the memory to 200M and the swap to 800M)
- _--oom-kill-disable_: OOM stands for Out Of Memory, makes it possible to avoid that an instance crashes if it exceeds the allocated memory allocation, can be practical for a greedy application.

Other options exist,
```shell
$ docker container run --help | grep -Ei 'mem|oom'
      --cpuset-mems string             MEMs in which to allow execution (0-3, 0,1)
      --kernel-memory bytes            Kernel memory limit
  -m, --memory bytes                   Memory limit
      --memory-reservation bytes       Memory soft limit
      --memory-swap bytes              Swap limit equal to memory plus swap: '-1' to enable unlimited swap
      --memory-swappiness int          Tune container memory swappiness (0 to 100) (default -1)
      --oom-kill-disable               Disable OOM Killer
      --oom-score-adj int              Tune host's OOM preferences (-1000 to 1000)
```

In order to limit the memery for the ubuntu  image :
```shell
$ docker container run -it --rm -m 500M ubuntu
root@1044c936c20a:/# free -h
             total       used       free     shared    buffers     cached
Mem:           23G       7.8G        15G        97M       457M       1.6G
-/+ buffers/cache:       5.8G        17G
Swap:           0B         0B         0B
```
We see no difference with the host machine, in fact the limitation is in the container process.

We will use stress for our tests, we will stress the container with 500M, which should represent around 50% of my machine's memory:
```shell
root@1044c936c20a:/# apt-get update && apt-get install stress
[...]
root@1044c936c20a:/# stress --vm 1 --vm-bytes 500M &
[1] 54
root@1044c936c20a:/# stress: info: [54] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd

root@1044c936c20a:/# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.1  20244  1644 ?        Ss   15:08   0:00 /bin/bash
root        54  0.0  0.0   7172     0 ?        S    15:12   0:00 stress --vm 1 --vm-bytes 500M
root        55 70.5 49.7 519176 506868 ?       R    15:12   0:04 stress --vm 1 --vm-bytes 500M
root        56  0.0  0.1  17500  1992 ?        R+   15:13   0:00 ps aux
root@1044c936c20a:/# kill 54 55
root@1044c936c20a:/#
```
We see that it is correct. Now let's test with 900M of RAM :
```shell
root@1044c936c20a:/# stress --vm 1 --vm-bytes 900M &
[1] 68
root@1044c936c20a:/# stress: info: [68] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd

root@1044c936c20a:/# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.1  20244  1696 ?        Ss   15:08   0:00 /bin/bash
root        68  0.0  0.0   7172     0 ?        S    15:15   0:00 stress --vm 1 --vm-bytes 900M
root        69 77.6 47.2 928776 482072 ?       D    15:15   0:02 stress --vm 1 --vm-bytes 900M
root        70  0.0  0.1  17500  2016 ?        R+   15:15   0:00 ps aux
root@1044c936c20a:/# kill 68 69
```
As you can see, stress uses only 47.2% of the memory available on the host, while it is told to use about 90%. So we see that the limitation works.

But I you are septic, we will test this same order in an unlimited container:
```shell
$ docker container run -ti --rm ubuntu
root@e3ba516add9a:/# apt-get update && apt-get install stress
[...]
root@e3ba516add9a:/# stress --vm 1 --vm-bytes 900M &
[1] 51
root@e3ba516add9a:/# stress: info: [51] dispatching hogs: 0 cpu, 0 io, 1 vm, 0 hdd
root@e3ba516add9a:/# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.1  20244  1584 ?        Ss   15:17   0:00 /bin/bash
root        51  0.0  0.0   7172     0 ?        S    15:18   0:00 stress --vm 1 --vm-bytes 900M
root        52 41.1 77.0 928776 785696 ?       D    15:18   0:03 stress --vm 1 --vm-bytes 900M
root        54  0.0  0.1  17500  1940 ?        R+   15:18   0:00 ps aux
```

## Limiting the CPU consumption
To limit the CPU we will use 3 options: 
  1. `-c -cpu-shares`: Allows the sharing of CPU resources, it is a proportion, if we put all the containers at 1024, they will share the resources equitably, if a container at 1024 and two others at 512, this will give 50% for container 1 and 25% for the other 
  2. `--cpu-quota`: Allows the limitation of CPU usage (50,000 for 50%, 0 for no limit) 
  3. `--cpuset-cpus`: Allows you to choose the CPU / core used (0,1 uses cpus 0 and 1 , 0-2 uses cpus 0, 1, and 2)

There are other options you can find with ``:

```shell
    docker container run --help | grep -E 'cpu'
      --cpu-period int                 Limit CPU CFS (Completely Fair Scheduler) period
      --cpu-quota int                  Limit CPU CFS (Completely Fair Scheduler) quota
      --cpu-rt-period int              Limit CPU real-time period in microseconds
      --cpu-rt-runtime int             Limit CPU real-time runtime in microseconds
      -c, --cpu-shares int                 CPU shares (relative weight)
      --cpus decimal                   Number of CPUs
      --cpuset-cpus string             CPUs in which to allow execution (0-3, 0,1)
      --cpuset-mems string             MEMs in which to allow execution (0-3, 0,1)
```

We will also use ubuntu with stress here:
```shell
$ docker container run -it --rm --cpus 0.25 ubuntu
root@0cfcada740e1:/# apt-get update && apt-get install stress
root@0cfcada740e1:/# stress -c 4 &
stress: info: [75] dispatching hogs: 4 cpu, 0 io, 0 vm, 0 hdd
```
Let's see the stats of docker
```shell
$ docker stats
CONTAINER ID        NAME                CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
0dee4a25261d        priceless_jang      25.40%              2.34MiB / 1.214GiB    0.19%               18.2MB / 154kB      0B / 26.2MB         6
```

